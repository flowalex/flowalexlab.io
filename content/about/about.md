---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

I am a System Engineer from Saint Paul MN. In my free time, I enjoy cooking, biking, and the outdoors.

## Socials:
[{{< image src=/images/github.svg  set=left >}}](https://github.com/flowalex-tech)
[{{< image src=/images/dev-dot-to.svg  set=left >}}](https://dev.to/flowalextech) [{{< image src=/images/twitter.svg  set=left >}}](https://twitter.com/AlexanderPWolf) [{{< image src=/images/youtube.svg  set=left >}}](https://www.youtube.com/channel/https://www.youtube.com/channel/UCzwTQBGzBH-2si_cMtPhFGw) [{{< image src=/images/reddit.svg  set=left >}}](https://www.reddit.com/user/flowalex999) [{{< image src=/images/gitlab.svg  set=left >}}](https://gitlab.com/flowalex)
