---
title: Free Tech Conferences 5 June 2021
date: 2021-06-05
---

## Upcoming Events:

### [SRE From Anywhere](https://www.catchpoint.com/sre/from-anywhere-2021)


### [GrafanaCONline 2021](https://grafana.com/about/events/grafanacon/2021/)


## Previous Events

### [New Relic FutureStack](https://newrelic.com/futurestack)

You will have to register for it, but all talks are available on-line

### [PYCon 2021](https://us.pycon.org/2021/schedule/talks/)

All of the videos have been uploaded on their [Youtube Channel](https://www.youtube.com/c/PyConUS/videos), but the descriptions are better on the event website

### [Failover Conf](https://gremlin.registration.goldcast.io/events/207e0873-4af1-49f4-b437-a291b3d29a6a)
